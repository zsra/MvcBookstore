﻿USE [Bookstore]
GO

/****** Object: Table [dbo].[Categories] Script Date: 24/08/2018 06:28:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

INSERT INTO dbo.Books (Title,CoverImage,[Description],Author,Price,CategoryId) 
VALUES ('Book1', '', 'This is the first book', 'Best author', 9.11,1),
	('Book2', '', 'This is the second book', 'Best author2', 6.11,2),
	('Book3', '', 'This is the third book', 'Best author3', 11.99,3)