﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Data.Models
{
    public class ShoppingCartItem
    {
        public int ShoppingCartItemId { get; set; }
        public Book Book { get; set; }
        public int Quantity { get; set; }
        public string ShoppingCartId { get; set; }
    }
}
