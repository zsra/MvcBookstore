﻿using Bookstore.Data.Database;
using Bookstore.Data.Interfaces;
using Bookstore.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Services
{
    public class BookService : IBook
    {
        private readonly BookstoreDbContext _ctx;

        public BookService(BookstoreDbContext context)
        {
            this._ctx = context;
        }

        public IEnumerable<Book> GetBooks() => _ctx.Books;
    }
}
