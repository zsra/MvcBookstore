﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bookstore.Data.Interfaces;
using Bookstore.Models;
using Bookstore.Services;
using Microsoft.AspNetCore.Mvc;

namespace Bookstore.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly IBook _book_ctx;
        private readonly ShoppingCart _cart;

        public ShoppingCartController(IBook book_ctx, ShoppingCart cart)
        {
            this._book_ctx = book_ctx;
            this._cart = cart;
            
        }

        public IActionResult Index()
        {
            var items = _cart.GetShoppingCartItems();

            var result = new ShoppingCartViewModel
            {
                ShoppingCart = _cart,
                Total = _cart.GetTotal()
            };

            return View(result);
        }

        public RedirectToActionResult AddToShoppingCart(int bookid)
        {
            var books = _book_ctx.GetBooks().FirstOrDefault(b => b.BookId == bookid);

            if(books != null)
            {
                _cart.AddToCart(books);
            }

            return RedirectToAction("Index");
        }

        public RedirectToActionResult RemoveFromShoppingCart(int bookid)
        {
            var books = _book_ctx.GetBooks().FirstOrDefault(b => b.BookId == bookid);

            if (books != null)
            {
                _cart.RemoveFromCart(books);
            }
            return RedirectToAction("Index");
        }
    }
}