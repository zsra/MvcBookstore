﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bookstore.Data.Database;
using Bookstore.Data.Interfaces;
using Bookstore.Data.Models;
using Bookstore.Models;
using Bookstore.Services;
using Microsoft.AspNetCore.Mvc;

namespace Bookstore.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrder _order;
        private readonly ShoppingCart _cart;
        private readonly BookstoreDbContext _context;

        public OrderController(IOrder order, ShoppingCart cart, BookstoreDbContext context)
        {
            this._order = order;
            this._cart = cart;
            this._context = context;
        }

        public IActionResult Index()
        {
            var orders = new OrderViewModel()
            {
                Orders = _context.Orders,
                OrderDetails = _context.OrderDetails        
            };

            return View(orders);

        }

        public IActionResult OrderInfo(int id)
        {

            return View(_context.Orders.FirstOrDefault(o => o.OrderId == id));

        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Order order)
        {
            var items = _cart.GetShoppingCartItems();
            _cart.ShoppingCartItems = items;

            if (_cart.ShoppingCartItems.Count == 0)
            {
                ModelState.AddModelError("", "You cart is empty");
            }

            if(ModelState.IsValid)
            {
                _order.Create(order);
                _cart.ClearCart();
            }
            

            return RedirectToAction("Index", "Books");
        }

    }
}